//
//  LocalStorageService.m
//  XchangeApp
//
//  Created by Anvar Basharov on 24.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "LocalStorageService.h"
#import "User.h"
#import "Currency.h"
#import "Account.h"

NSString * const kUser = @"kUser";

@implementation LocalStorageService

- (void)loadUser:(void(^)(User* user))completion {
    NSData* data =  [[NSUserDefaults standardUserDefaults] objectForKey:kUser];
    if (data) {
        User* user = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        completion(user);
        return;
    } else {
        User* user = [self defaultUserWithMainCurrency:EUR];
        [self saveUser:user];
        completion(user);
    }
}

- (void)saveUser:(User *)user {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:user];
    [[NSUserDefaults standardUserDefaults] setObject:encodedObject forKey:kUser];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (User *)defaultUserWithMainCurrency:(CurrencyType)curencyType {
    
    NSArray* currencies = @[@(EUR),@(USD),@(GBP)];
    
    NSMutableArray* accountsArray = [NSMutableArray array];
    for (NSNumber* currCode in currencies) {
        Currency* currency = [Currency new];
        currency.currencyType = currCode.integerValue;
        currency.symbol = [Currency symbolForCurrency:currCode.integerValue];
        currency.code = [Currency stringCodeForCurrency:currCode.integerValue];
        
        if (currency.currencyType == curencyType) { currency.rate = [NSDecimalNumber one];}
        
        Account* acount = [Account new];
        acount.balance = [NSDecimalNumber decimalNumberWithString:@"100.00"];
        acount.currency = currency;
        [accountsArray addObject:acount];
    }
    User* user = [User new];
    user.accountsArray = accountsArray;
    return user;
}

@end
