//
//  LocalStorageServiceProtocol.h
//  XchangeApp
//
//  Created by Anvar Basharov on 24.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class User;

@protocol LocalStorageServiceProtocol <NSObject>

- (void)loadUser:(void(^)(User* user))completion;
- (void)saveUser:(User *)user;

@end
