//
//  ExchangeRatesServiceProtocol.h
//  XchangeApp
//
//  Created by Anvar Basharov on 24.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Error;
@class Currency;

@protocol ExchangeRatesServiceProtocol <NSObject>

- (NSURLSessionDataTask *)getCurrencyRates:(void(^)(NSArray<Currency*>*currencciesArray))success
                                   failure:(void(^)(Error* error))failure;

@end
