//
//  ExchangeRatesService.h
//  XchangeApp
//
//  Created by Anvar Basharov on 24.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ExchangeRatesServiceProtocol.h"

@interface ExchangeRatesService : NSObject <ExchangeRatesServiceProtocol>

@end
