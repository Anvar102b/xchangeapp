//
//  ExchangeRatesService.m
//  XchangeApp
//
//  Created by Anvar Basharov on 24.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "ExchangeRatesService.h"
#import "URLRequest.h"
#import "WebClient.h"
#import "Error.h"
#import "CurrencyMapper.h"

#import "XMLParser.h"


NSString *const eurofxref = @"/stats/eurofxref/eurofxref-daily.xml";

@implementation ExchangeRatesService

- (NSURLSessionDataTask *)getCurrencyRates:(void(^)(NSArray <Currency*>*currencciesArray))success
                                   failure:(void(^)(Error* error))failure {
    NSString* urlString = [NSString stringWithFormat:@"%@%@", [WebClient baseUrl], eurofxref];
    URLRequest* request = [[URLRequest alloc] initWithMethod:GET urlString:urlString parameters:nil];
    WebClient* webClient = [WebClient new];
    NSURLSessionDataTask* task = [webClient makeRequest:request completion:^(NSData *data, NSURLResponse *response, Error *error) {
        if (data) {
            XMLParser* xmlParser = [XMLParser new];
            [xmlParser parseData:data success:^(id parsedData) {
                NSArray* currencies = [CurrencyMapper mapRatesArrayWithDict:parsedData];
                success(currencies);
            } failure:^(NSError *error) {
                Error* parseError = [Error errorWithDomain:error.domain code:error.code userInfo:error.userInfo];
                [parseError setMessage:@"XML parse failed"];
                failure(parseError);
            }];
        }
        if (error) {
            failure(error);
        }
    }];
    return task;
}

@end
