//
//  Types.h
//  XchangeApp
//
//  Created by Anvar Basharov on 24.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#ifndef Types_h
#define Types_h

typedef enum : NSUInteger {
    EUR,
    USD,
    GBP,
} CurrencyType;

// UI Types

typedef enum : NSUInteger {
    FromAccountCellType,
    ArrowCellType,
    ToAccountCellType,
} CellType;

#endif /* Types_h */
