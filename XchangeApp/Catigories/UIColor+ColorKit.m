//
//  UIColor+ColorKit.m
//  XchangeApp
//
//  Created by Anvar Basharov on 25.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "UIColor+ColorKit.h"

@implementation UIColor (ColorKit)

+ (UIColor* )graySky {
    return [UIColor colorWithRed:0.95 green:0.96 blue:0.96 alpha:1.00];
}

+ (UIColor *)deepBlue {
    return [UIColor colorWithRed:0.00 green:0.43 blue:0.66 alpha:1.00];
}

+ (UIColor *)shadowColor {
    return [UIColor colorWithRed:(45.f / 255.f) green:(70.f / 255.f) blue:(104.f / 255.f) alpha:0.2f];
}

@end
