//
//  NSString+URL.h
//  Modulbank
//
//  Created by Anvar Basharov on 24.08.17.
//  Copyright © 2017 Сергей Николаев. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (URL)

+ (NSString *)urlWithParameters:(NSDictionary *)parameters;

@end
