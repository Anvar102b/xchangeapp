//
//  NSString+NumbersValidation.h
//  XchangeApp
//
//  Created by Anvar Basharov on 04.10.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NumbersValidation)

- (NSString *)allowOnlyNumbersAndPoints;

- (NSString *)validateForZero;

- (NSString *)addPointIfNeeded;

@end
