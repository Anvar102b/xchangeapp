//
//  NSString+NumbersValidation.m
//  XchangeApp
//
//  Created by Anvar Basharov on 04.10.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "NSString+NumbersValidation.h"

NSString* const  kPattern = @"[0-9]+(.[0-9][0-9]?)?";

@implementation NSString (NumbersValidation)

- (NSString *)allowOnlyNumbersAndPoints {
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:kPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSMutableArray* mArray = [NSMutableArray array];
    [regex enumerateMatchesInString:self options:0 range:NSMakeRange(0, [self length]) usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop){
        [mArray addObject:[self substringWithRange:match.range]];
    }];
    NSString* cropped = mArray.count > 0 ? mArray[0] : @"";
    return cropped;
}

- (NSString *)validateForZero {
    NSString* fString;
    
    if ([self isEqualToString:@"00"] || [self isEqualToString:@"0"]) {
        fString = @"0.";
    } else {
        fString = self;
    }
    
    if (self.length > 1 && [[self substringToIndex:1] isEqualToString:@"0"] && ![[self substringWithRange:NSMakeRange(1, 1)] isEqualToString:@"."]) {
        fString = [self substringFromIndex:1];
    }
    
    return fString;
}

- (NSString *)addPointIfNeeded {
    if ([self rangeOfString:@"."].location != NSNotFound || self.length == 0) {
        return self;
    }
    return [NSString stringWithFormat:@"%@.", self];
    
}

@end
