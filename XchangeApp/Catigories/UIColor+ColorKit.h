//
//  UIColor+ColorKit.h
//  XchangeApp
//
//  Created by Anvar Basharov on 25.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ColorKit)

+ (UIColor* )graySky;

+ (UIColor *)deepBlue;

+ (UIColor *)shadowColor;

@end
