//
//  NSString+URL.m
//  Modulbank
//
//  Created by Anvar Basharov on 24.08.17.
//  Copyright © 2017 Сергей Николаев. All rights reserved.
//

#import "NSString+URL.h"

@implementation NSString (URL)

+ (NSString *)urlWithParameters:(NSDictionary *)parameters {
    NSMutableString *urlString = [NSMutableString stringWithFormat:@"?"];
    NSArray *keys = parameters.allKeys;
    for (id object in keys) {
        if ([parameters[object] isKindOfClass:[NSArray class]]) {
            for (id obj in parameters[object])
                [urlString appendString:[NSString stringWithFormat:@"%@=%@&", object, obj]];
        } else {
            [urlString appendString:[NSString stringWithFormat:@"%@=%@&", object, parameters[object]]];
        }
    }
    return urlString;
}


@end
