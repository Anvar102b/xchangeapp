//
//  Error.m
//  XchangeApp
//
//  Created by Anvar Basharov on 23.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "Error.h"

@interface Error ()

@property (nonatomic, strong) NSString* message;

@end

@implementation Error

- (void)setMessage:(NSString *)message {
    _message = message;
}

- (NSString *)localizedDescription {
    NSString* message;
    switch (self.errorType) {
        case NoInternetConnection:
            message = @"No Internet connection";
            break;
        case Custom:
            message = _message;
            break;
        default:
            break;
    }
    return message;
}

@end
