//
//  Error.h
//  XchangeApp
//
//  Created by Anvar Basharov on 23.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    NoInternetConnection,
    Custom,
} ServiceErrorType;

@interface Error : NSError

@property (nonatomic, assign) ServiceErrorType errorType;

- (void)setMessage:(NSString *)message;

@end


