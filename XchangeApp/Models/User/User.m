//
//  User.m
//  XchangeApp
//
//  Created by Anvar Basharov on 24.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "User.h"

@implementation User

#pragma mark - NSCoding

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_accountsArray forKey:@"accountsArray"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init]; if (!self) { return nil; }
    _accountsArray = [aDecoder decodeObjectForKey:@"accountsArray"];
    return self;
}

@end
