//
//  User.h
//  XchangeApp
//
//  Created by Anvar Basharov on 24.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Account;

@interface User : NSObject <NSCoding>

@property (nonatomic, strong) NSArray<Account*>* accountsArray;

@end
