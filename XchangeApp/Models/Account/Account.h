//
//  Account.h
//  XchangeApp
//
//  Created by Anvar Basharov on 24.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Currency;

@interface Account : NSObject <NSCoding>

@property (nonatomic, strong) Currency* currency;
@property (nonatomic, strong) NSDecimalNumber* balance;

@end
