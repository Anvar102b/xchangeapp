//
//  Account.m
//  XchangeApp
//
//  Created by Anvar Basharov on 24.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "Account.h"

@implementation Account

#pragma mark - NSCoding

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_currency forKey:@"currency"];
    [aCoder encodeObject:_balance forKey:@"balance"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init]; if (!self) { return nil; }
    _currency = [aDecoder decodeObjectForKey:@"currency"];
    _balance = [aDecoder decodeObjectForKey:@"balance"];
    return self;
}


@end
