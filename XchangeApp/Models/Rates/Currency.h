//
//  Rate.h
//  XchangeApp
//
//  Created by Anvar Basharov on 24.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Types.h"

@interface Currency : NSObject <NSCoding>

@property (nonatomic, assign) CurrencyType currencyType;
@property (nonatomic, strong) NSDecimalNumber* rate;
@property (nonatomic, strong) NSString* symbol;
@property (nonatomic, strong) NSString* code;

+ (NSString *)stringCodeForCurrency:(CurrencyType)currency;
+ (NSString *)symbolForCurrency:(CurrencyType)currency;
+ (CurrencyType)currencyTypeForString:(NSString *)currency;


@end
