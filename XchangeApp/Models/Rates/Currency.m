//
//  Rate.m
//  XchangeApp
//
//  Created by Anvar Basharov on 24.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "Currency.h"

@implementation Currency

#pragma mark - NSCoding

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeInteger:_currencyType forKey:@"currencyType"];
    [aCoder encodeObject:_rate forKey:@"rate"];
    [aCoder encodeObject:_symbol forKey:@"symbol"];
    [aCoder encodeObject:_code forKey:@"code"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super init]; if (!self) { return nil; }
    _currencyType = [aDecoder decodeIntegerForKey:@"currencyType"];
    _rate = [aDecoder decodeObjectForKey:@"rate"];
    _symbol = [aDecoder decodeObjectForKey:@"symbol"];
    _code = [aDecoder decodeObjectForKey:@"code"];
    return self;
}


+ (NSString *)symbolForCurrency:(CurrencyType)currency {
    NSDictionary<NSNumber *, NSString *> *currencySymbols = @{
                                                              @(GBP) : @"£",
                                                              @(EUR) : @"€",
                                                              @(USD) : @"$"
                                                              };
    return currencySymbols[@(currency)];
}

+ (NSString *)stringCodeForCurrency:(CurrencyType)currency {
    NSDictionary<NSNumber *, NSString *> *currencyTypes = @{
                                                              @(GBP) : @"GBP",
                                                              @(EUR) : @"EUR",
                                                              @(USD) : @"USD"
                                                              };
    return currencyTypes[@(currency)];
}

+ (CurrencyType)currencyTypeForString:(NSString *)currency {
    NSDictionary* currencyTypes = @{@"EUR" : @(EUR),
                                    @"USD" : @(USD),
                                    @"GBP" : @(GBP)
                                    };
    return  [currencyTypes[currency] integerValue];
}


@end
