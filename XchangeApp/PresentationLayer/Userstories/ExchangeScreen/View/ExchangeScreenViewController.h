//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExchangeScreenViewInput.h"

@protocol ExchangeScreenViewOutput;

@interface ExchangeScreenViewController : UIViewController <ExchangeScreenViewInput>

@property (nonatomic, strong) id <ExchangeScreenViewOutput> presenter;

@end
