//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Types.h"

@protocol ExchangeScreenViewInput <NSObject>

- (void)reloadCells:(CellType)cellType;
- (void)showAlertWithMessage:(NSString *)message;
- (void)updateTitle:(NSString *)title;
- (void)showNoInternetAlert:(NSString *)text;

@end
