//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import "ExchangeScreenViewController.h"
#import "ExchangeScreenPresenter.h"

#import "AccountsCell.h"
#import "ArrowCell.h"

#import "ExchangeScreenViewOutput.h"

#import "Types.h"
#import "UIColor+ColorKit.h"

typedef enum : NSUInteger {
    MBKeyboardHidden,
    MBKeyboardShown,
} MBKeyboardState;

@interface ExchangeScreenViewController () <UITableViewDelegate, UITableViewDataSource, AccountsCellDelegate, AccountsCellDataSource>

@property (nonatomic, weak) IBOutlet UITableView* tableView;

@property (nonatomic, assign) MBKeyboardState keyboardState;
@property (nonatomic, strong) NSArray* caruselsArray;

@end

@implementation ExchangeScreenViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.presenter obtainUser];
    [self.presenter obtainCurrencies];
    
    [self initObjects];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willEnterForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)initObjects {
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.allowsSelection = NO;
    self.tableView.backgroundColor = [UIColor graySky];
    self.tableView.contentInset = UIEdgeInsetsMake(10.f, 0.f, 0.f, 0.f);
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 40.f;
    [self.view addSubview:self.tableView];
    
    UIBarButtonItem *exchangeItem = [[UIBarButtonItem alloc] initWithTitle:@"Exchange" style:UIBarButtonItemStylePlain target:self
                                                                action:@selector(exchangeAction:)];
    exchangeItem.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = exchangeItem;
    
}

#pragma mark - Getters and Setters

- (NSArray *)caruselsArray {
    if(!_caruselsArray){
        _caruselsArray = @[@(FromAccountCellType),@(ArrowCellType),@(ToAccountCellType)];
    }
    return _caruselsArray;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.caruselsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id cell;
    switch (indexPath.row) {
        case FromAccountCellType:
        case ToAccountCellType:{
            AccountsCell *accountsCell = [self.tableView dequeueReusableCellWithIdentifier:@"AccountsCell" forIndexPath:indexPath];
            NSArray* accountsArray = [self.presenter accountsArray];
            NSString* fromAmount = [self.presenter stringFromAmount];
            NSString* toAmount = [self.presenter stringToAmount];
            
            [accountsCell configureWithAccounts:accountsArray fromAmount:fromAmount toAmount:toAmount type:indexPath.row];
            accountsCell.dataSource = self;
            accountsCell.delegate = self;
            cell = accountsCell;
            break;
        }
        case ArrowCellType:
            cell = [self.tableView dequeueReusableCellWithIdentifier:@"ArrowCell" forIndexPath:indexPath];
            break;
        default:
            break;
    }
    
    
    return cell;
}

#pragma mark - ExchangeScreenViewInput

- (void)reloadCells:(CellType)cellType {
    NSArray* accountsArray = [self.presenter accountsArray];
    NSString* fromAmount = [self.presenter stringFromAmount];
    NSString* toAmount = [self.presenter stringToAmount];
    switch (cellType) {
        case FromAccountCellType: {
            AccountsCell *fromCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:FromAccountCellType inSection:0]];
            [fromCell configureWithAccounts:accountsArray fromAmount:fromAmount toAmount:toAmount type:FromAccountCellType];
            break;
        }
        case ToAccountCellType: {
            AccountsCell *toCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:ToAccountCellType inSection:0]];
            [toCell configureWithAccounts:accountsArray fromAmount:fromAmount toAmount:toAmount type:ToAccountCellType];
            break;
        }
        default:
            break;
    }
}

- (void)showAlertWithMessage:(NSString *)message {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)updateTitle:(NSString *)title {
    self.title = title;
}

- (void)showNoInternetAlert:(NSString *)text {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil message:text preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.presenter obtainCurrencies];
    }];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - AccountsCellDelegate

- (NSString *)rateStringForAccountAtIndex:(NSInteger)index {
    return [self.presenter rateStringForAccountAtIndex:index];
}

- (void)didBeginEditing:(UITableViewCell *)cell {
    
    NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
}

- (void)showErrorAlertWithText:(NSString *)text {
    [self showAlertWithMessage:text];
}

#pragma mark - AccountsCellDataSource

- (void)carouselCurrentItemIndexDidChange:(NSInteger)index cell:(UITableViewCell *)cell {
    NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
    switch (indexPath.row) {
        case FromAccountCellType:
            [self.presenter didChangeFromAccountView:index];
            break;
        case ToAccountCellType:
            [self.presenter didChangeToAccountView:index];
            break;
        default:
            break;
    }
}

- (void)inputAmountDidChange:(NSDecimalNumber *)inputNumber cell:(UITableViewCell *)cell {
    NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];    
    switch (indexPath.row) {
        case FromAccountCellType: {
            [self.presenter inputAmountForFromAccountDidChangeAmount:inputNumber];
            break;
        }
        case ToAccountCellType: {
            [self.presenter inputAmountForToAccountDidChangeAmount:inputNumber];
            break;
        }
        default:
            break;
    }
}

#pragma mark - Actions

- (void)exchangeAction:(UIBarButtonItem *)sender {
    [self.presenter exchangeActionTapped];
}

#pragma mark - Notification

- (void)keyboardWillHide:(NSNotification *)notification {
    self.tableView.contentInset = UIEdgeInsetsMake(10.f, 0.f, 0.f, 0.f);
    self.keyboardState = MBKeyboardHidden;
}

- (void)keyboardWillShow:(NSNotification *)notification {
    CGFloat height = [[notification userInfo][UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    self.tableView.contentInset = UIEdgeInsetsMake(10.f, 0.f, height + 10.f, 0.f);
    self.keyboardState = MBKeyboardShown;
}

- (void)willEnterForeground:(NSNotificationCenter *)notification {
    [self.presenter willEnterForeground];
}

- (void)didEnterBackground:(NSNotificationCenter *)notification {
    [self.presenter didEnterBackground];
}

@end
