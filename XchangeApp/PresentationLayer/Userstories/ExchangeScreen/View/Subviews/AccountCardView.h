//
//  AccountCard.h
//  XchangeApp
//
//  Created by Anvar Basharov on 25.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Types.h"

@protocol AccountCardViewDelegate

- (NSString *)rateForOneUnit;
- (CellType)inputCellType;
- (void)inputAmountDidChange:(NSNumber *)inputAmount;
- (void)showErrorAlertWithText:(NSString *)text;

@end;

@class Account;

@interface AccountCardView : UIView

@property (nonatomic, weak) id <AccountCardViewDelegate>  delegate;

- (void)configureWith:(Account *)account cellType:(CellType)cellType inputAmount:(NSString *)inputAmount;

@end
