//
//  AccountCard.m
//  XchangeApp
//
//  Created by Anvar Basharov on 25.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "AccountCardView.h"
#import "UIColor+ColorKit.h"

#import "Account.h"
#import "Currency.h"

#import "NSString+NumbersValidation.h"

NSString* const  kCreditSum = @"- ";
NSString* const  kDebetSum = @"+ ";

NSUInteger kMaxInputLength = 10;

@interface AccountCardView () <UITextFieldDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, weak) IBOutlet UIView* contentView;
@property (nonatomic, weak) IBOutlet UILabel* currencyCodeLabel;
@property (nonatomic, weak) IBOutlet UILabel* balanceLabel;
@property (nonatomic, weak) IBOutlet UILabel* ratelabel;
@property (nonatomic, weak) IBOutlet UITextField* inputTextfield;

@end

@implementation AccountCardView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor graySky];
    _contentView.backgroundColor = [UIColor whiteColor];
    _contentView.layer.cornerRadius = 2.f;
    _contentView.layer.shadowColor = [UIColor shadowColor].CGColor;
    _contentView.layer.shadowRadius = 2.f;
    _contentView.layer.shadowOpacity = 1.f;
    _contentView.layer.shadowOffset = CGSizeMake(1.f, 1.f);
    _inputTextfield.delegate = self;
}

- (nullable UIView *)hitTest:(CGPoint)point withEvent:(nullable UIEvent *)event {
    BOOL pointInside = [self pointInside:point withEvent:event];
    return pointInside ? self.superview : [super hitTest:point withEvent:event];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString* fString = [[NSString stringWithFormat:@"%@%@", textField.text, string] allowOnlyNumbersAndPoints];
    if (fString.length > kMaxInputLength && ![string isEqualToString:@""]) {
        [self.delegate showErrorAlertWithText:[NSString stringWithFormat:@"Maximum input length %lu", (unsigned long)kMaxInputLength]];
        return NO;
    }
    
    if (string.length == 0) {
        if (textField.text.length == 3) {
            if ([textField.text hasPrefix:kCreditSum] || [textField.text hasPrefix:kDebetSum]) {
                textField.text = @"";
            }
        } else {
            NSMutableString* mString = [NSMutableString stringWithString:textField.text];
            if (range.location > 1) {
                [mString deleteCharactersInRange:range];
                textField.text = mString;
            }
        }
    }
    
    if (string.length > 0) {
        if ([textField.text hasPrefix:kCreditSum] || [textField.text hasPrefix:kDebetSum]) {
            if (range.location <= 1) {
                return NO;
            }
        }
        
        string = [string stringByReplacingOccurrencesOfString:@"," withString:@"."];
        NSString* finalString;
        NSMutableString* mString = [NSMutableString stringWithString:textField.text];
        [mString insertString:string atIndex:range.location];
        finalString = mString;
        finalString = [finalString allowOnlyNumbersAndPoints];
        finalString = [finalString validateForZero];
        
        if ([string isEqualToString:@"."]) {
            finalString = [finalString addPointIfNeeded];
        }
        
        switch ([self.delegate inputCellType]) {
            case FromAccountCellType: {
                textField.text = finalString.length > 0 ? [NSString stringWithFormat:@"%@%@",kCreditSum, finalString] : @"";
                break;
            }
            case ToAccountCellType: {
                textField.text = finalString.length > 0 ? [NSString stringWithFormat:@"%@%@",kDebetSum, finalString] : @"";
                break;
            }
            default:
                break;
        }
    }
    [self prepareAndPushStringForUpdateAccount:[textField.text allowOnlyNumbersAndPoints]];
    return NO;
}




#pragma mark - Public

- (void)configureWith:(Account *)account cellType:(CellType)cellType inputAmount:(NSString *)inputAmount; {
    
    self.currencyCodeLabel.text = account.currency.code;
    NSString* balance = [NSString stringWithFormat:@"You have: %.02f%@",account.balance.doubleValue, account.currency.symbol];
    self.balanceLabel.text =  balance;
    
    [self.delegate rateForOneUnit] != nil ? (self.ratelabel.alpha = 1.f) : (self.ratelabel.alpha = 0.f);
    self.ratelabel.text = [NSString stringWithFormat:@"%@1 = %@", account.currency.symbol, [self.delegate rateForOneUnit]];
    [self.delegate rateForOneUnit] != nil  ? (self.inputTextfield.enabled = YES) : (self.inputTextfield.enabled = NO);
    
    switch (cellType) {
        case FromAccountCellType: {
            if (inputAmount) {
                self.inputTextfield.text = [NSString stringWithFormat:@"%@%.02f", kCreditSum, inputAmount.doubleValue];
            } else {
                self.inputTextfield.text = @"";
            }
            break;
        }
        case ToAccountCellType: {
            if (inputAmount){
                self.inputTextfield.text = [NSString stringWithFormat:@"%@%.02f",kDebetSum, inputAmount.doubleValue];
            } else {
                self.inputTextfield.text = @"";
            }
            break;
        }
        default:
            break;
    }
}

- (BOOL)becomeFirstResponder {
    return [self.inputTextfield becomeFirstResponder];
}

#pragma mark - Private

- (void)prepareAndPushStringForUpdateAccount:(NSString *)string {
    if (string && ![string isEqualToString:@""]) {
        string = [string stringByReplacingOccurrencesOfString:kDebetSum withString:@""];
        string = [string stringByReplacingOccurrencesOfString:kCreditSum withString:@""];
        NSDecimalNumber* inputNumber = [NSDecimalNumber decimalNumberWithString:string];
        [self.delegate inputAmountDidChange:inputNumber];
    } else {
        [self.delegate inputAmountDidChange:nil];
    }
}

@end
