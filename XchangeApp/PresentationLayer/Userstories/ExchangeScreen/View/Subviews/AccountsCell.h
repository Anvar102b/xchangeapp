//
//  AccountsCell.h
//  XchangeApp
//
//  Created by Anvar Basharov on 25.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Types.h"

@protocol AccountsCellDelegate

- (void)carouselCurrentItemIndexDidChange:(NSInteger)index cell:(UITableViewCell *)cell;
- (void)inputAmountDidChange:(NSDecimalNumber *)inputNumber cell:(UITableViewCell *)cell;
- (void)didBeginEditing:(UITableViewCell *)cell;
- (void)showErrorAlertWithText:(NSString *)text;

@end

@protocol AccountsCellDataSource

- (NSString *)rateStringForAccountAtIndex:(NSInteger)index;

@end

@interface AccountsCell : UITableViewCell

@property (nonatomic, weak) id <AccountsCellDelegate> delegate;
@property (nonatomic, weak) id <AccountsCellDataSource> dataSource;

- (void)configureWithAccounts:(NSArray*)accountsArray fromAmount:(NSString *)fromAmount toAmount:(NSString *)toAmount type:(CellType)cellType;

@end
