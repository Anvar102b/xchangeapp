//
//  AccountsCell.m
//  XchangeApp
//
//  Created by Anvar Basharov on 25.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "AccountsCell.h"
#import "AccountCardView.h"
#import "iCarousel.h"
#import "Account.h"

#import "UIColor+ColorKit.h"

@interface AccountsCell () <iCarouselDelegate, iCarouselDataSource, AccountCardViewDelegate>

@property (nonatomic, weak) IBOutlet iCarousel* caruselView;

@property (nonatomic, strong) NSArray* accountsArray;
@property (nonatomic, strong) NSString* inputAmountString;
@property (nonatomic, assign) CellType cellType;

@end

@implementation AccountsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor = [UIColor graySky];
    self.caruselView.type = iCarouselTypeLinear;
    self.caruselView.delegate = self;
    self.caruselView.dataSource = self;
    self.caruselView.pagingEnabled = YES;
    self.caruselView.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void)configureWithAccounts:(NSArray*)accountsArray fromAmount:(NSString *)fromAmount toAmount:(NSString *)toAmount type:(CellType)cellType {
    self.accountsArray = accountsArray;
    self.cellType = cellType;
    switch (cellType) {
        case FromAccountCellType:
            self.inputAmountString = fromAmount;
            break;
        case ToAccountCellType:
            self.inputAmountString = toAmount;
            break;
        default:
            break;
    }

    if (self.caruselView.currentItemView) {
        [self.caruselView reloadItemAtIndex:self.caruselView.currentItemIndex animated:NO];
    } else {
        [self.caruselView reloadData];
    }
}

#pragma mark - iCarouselDataSource

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel {
    return self.accountsArray.count;
}
- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(nullable UIView *)view {
    
    AccountCardView* cardView = (AccountCardView *)view;
    if (!cardView) {
        cardView = [[[UINib nibWithNibName:NSStringFromClass([AccountCardView class]) bundle:nil] instantiateWithOwner:nil options:nil]firstObject];        
        CGRect frame = cardView.frame;
        frame.size.width = CGRectGetWidth(self.contentView.bounds) - 20.f;
        cardView.frame = frame;
    }
    cardView.delegate = self;
    Account* account = self.accountsArray[index];
    [cardView configureWith:account cellType:self.cellType inputAmount:self.inputAmountString];
    
    return cardView;
}

#pragma mark - iCarouselDelegate

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value {
    switch (option) {
        case iCarouselOptionWrap:
            return YES;
        default:
            return value;
    }
    return value;
}

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel{
    [self.delegate carouselCurrentItemIndexDidChange:carousel.currentItemIndex cell:self];
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index {
    AccountCardView* cardView = (AccountCardView *)[carousel itemViewAtIndex:index];
    [cardView becomeFirstResponder];
    [self.delegate didBeginEditing:self];
}

#pragma mark - AccountCardViewDelegate

- (CellType)inputCellType {
    return self.cellType;
}

- (NSString *)rateForOneUnit {
    NSInteger index = self.caruselView.currentItemIndex;
    return [self.dataSource rateStringForAccountAtIndex:index];
}

- (void)inputAmountDidChange:(NSDecimalNumber *)inputAmount {
    [self.delegate inputAmountDidChange:inputAmount cell:self];
}

- (void)showErrorAlertWithText:(NSString *)text {
    [self.delegate showErrorAlertWithText:text];
}

@end
