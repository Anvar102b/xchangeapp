//
//  ArrowCell.m
//  XchangeApp
//
//  Created by Anvar Basharov on 25.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "ArrowCell.h"
#import "UIColor+ColorKit.h"

@implementation ArrowCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor = [UIColor graySky];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        UIImageView* arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, 20.f, 20.f)];
        arrowImageView.image = [UIImage imageNamed:@"arrow"];
        arrowImageView.center = self.contentView.center;
        arrowImageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [self.contentView addSubview:arrowImageView];
        self.contentView.backgroundColor = [UIColor graySky];
    }
    return self;
    
}


@end
