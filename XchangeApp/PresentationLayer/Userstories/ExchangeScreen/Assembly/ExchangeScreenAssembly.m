//
//  ExchangeScreenAssembly.m
//  XchangeApp
//
//  Created by Anvar Basharov on 24.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "ExchangeScreenAssembly.h"
#import "ExchangeScreenViewController.h"
#import "ExchangeScreenPresenter.h"
#import "ExchangeScreenInteractor.h"

#import "ExchangeRatesService.h"
#import "LocalStorageService.h"

#import "User.h"


@implementation ExchangeScreenAssembly

+ (UINavigationController *)assemblyExchangeScreenModule {
    //Generating module components
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    UINavigationController * initialViewController = [mainStoryboard instantiateInitialViewController];

    ExchangeScreenViewController *view = initialViewController.viewControllers[0];
    ExchangeScreenPresenter *presenter = [ExchangeScreenPresenter new];
    ExchangeScreenInteractor *interactor = [ExchangeScreenInteractor new];
    
    id <ExchangeRatesServiceProtocol> ratesService = [ExchangeRatesService new];
    id <LocalStorageServiceProtocol> localStorage = [LocalStorageService new];
    
    // Connecting
    view.presenter = presenter;
    presenter.view = view;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    interactor.ratesService = ratesService;
    interactor.storageService = localStorage;
    
    return initialViewController;
}

@end
