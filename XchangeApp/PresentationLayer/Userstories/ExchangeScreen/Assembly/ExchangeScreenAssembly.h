//
//  ExchangeScreenAssembly.h
//  XchangeApp
//
//  Created by Anvar Basharov on 24.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ExchangeScreenAssembly : NSObject

+ (UINavigationController *)assemblyExchangeScreenModule;

@end
