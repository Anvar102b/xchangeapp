//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class User;
@class Account;
@class Currency;

@protocol ExchangeScreenInteractorInput <NSObject>

- (void)loadUser;

- (void)loadCurrencies;

- (void)saveUser:(User *)user;

- (void)transactionFromAccount:(Account *)fromAccount
                   inputAmount:(NSDecimalNumber *)amount
                     toAccount:(Account *)toAccount
                       success:(void(^)(Account* account, NSDecimalNumber *convertedNum))success
                       failure:(void(^)(NSError* error))failure;

- (NSDecimalNumber *)convertFromCurrency:(Account *)fromAccount
                             inputAmount:(NSDecimalNumber *)amount
                               toAccount:(Account *)toAccount;

- (NSDecimalNumber *)convertOneUnitFromCurrency:(Currency *)fromCurrency
                                     toCurrency:(Currency *)toCurrency;

- (BOOL)isTimerValid;

- (void)startUpdatingCurrencies;

- (void)invalidateTimer;

@end
