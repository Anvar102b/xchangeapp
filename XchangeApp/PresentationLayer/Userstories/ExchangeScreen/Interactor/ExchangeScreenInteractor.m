//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import "ExchangeScreenInteractor.h"
#import "ExchangeScreenPresenter.h"

#import "Account.h"
#import "Currency.h"
#import "Error.h"

#import "LocalStorageServiceProtocol.h"
#import "ExchangeScreenInteractorOutput.h"
#import "ExchangeRatesServiceProtocol.h"

@interface ExchangeScreenInteractor()

@property (nonatomic, strong) NSTimer* timer;

@end

@implementation ExchangeScreenInteractor

#pragma mark - ExchangeScreenInteractorInput

- (void)loadUser {
    
    [self.storageService loadUser:^(User *user) {
        [self.presenter loadedUser:user];
    }];
}
- (void)loadCurrencies {

    [self.ratesService getCurrencyRates:^(NSArray<Currency *> *currenciesArray) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.presenter loadedCurrencies:currenciesArray];
        });
    } failure:^(Error *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            switch (error.errorType) {
                case NoInternetConnection:
                    [self.presenter connectionFailed:error.localizedDescription];
                    break;
                case Custom:
                    [self.presenter haveError:error.localizedDescription];
                default:
                    break;
            }
        });
    }];
}

- (BOOL)isTimerValid {
    return self.timer.isValid;
}

- (void)startUpdatingCurrencies {
    self.timer = [NSTimer scheduledTimerWithTimeInterval:30.0
                                     target:self
                                   selector:@selector(loadCurrencies)
                                   userInfo:nil
                                    repeats:YES];
}

- (void)invalidateTimer {
    [self.timer invalidate];
}

- (void)saveUser:(User *)user; {
    [self.storageService saveUser:user];
}

- (NSDecimalNumber *)convertFromCurrency:(Account *)fromAccount
                             inputAmount:(NSDecimalNumber *)amount
                               toAccount:(Account *)toAccount {
    NSDecimalNumber *transactionAmount = [self convertFromAccount:fromAccount.currency.rate inputAmount:amount toAccount:toAccount.currency.rate];
    return transactionAmount;
}

- (NSDecimalNumber *)convertOneUnitFromCurrency:(Currency *)fromCurrency toCurrency:(Currency *)toCurrency {

    if (!fromCurrency.rate || !toCurrency.rate) {
        return nil;
    }
    NSDecimalNumber* oneUnit = [NSDecimalNumber one];
    NSDecimalNumber* transactionAmount = [self convertFromAccount:fromCurrency.rate inputAmount:oneUnit toAccount:toCurrency.rate];
    return transactionAmount;
}

- (void)transactionFromAccount:(Account *)fromAccount
                   inputAmount:(NSDecimalNumber *)amount
                     toAccount:(Account *)toAccount
                       success:(void(^)(Account* account, NSDecimalNumber *convertedNum))success
                       failure:(void(^)(NSError* error))failure {
    
    if (amount.doubleValue > fromAccount.balance.doubleValue) {
        NSError* error = [[NSError alloc] initWithDomain:@"Error" code:2017 userInfo:@{NSLocalizedDescriptionKey : @"Insufficient funds for replenishment" }];
        failure(error);
        return;
    }
    
    if ([fromAccount isEqual:toAccount]) {
        NSError* error = [[NSError alloc] initWithDomain:@"Error" code:2018 userInfo:@{NSLocalizedDescriptionKey : @"Please choose different accounts" }];
        failure(error);
        return;
    }
    
    if (amount.doubleValue == 0.f) {
        NSError* error = [[NSError alloc] initWithDomain:@"Error" code:2019 userInfo:@{NSLocalizedDescriptionKey : @"Please enter amount" }];
        failure(error);
        return;
    }
    NSDecimalNumber *transactionAmount = [self convertFromAccount:fromAccount.currency.rate inputAmount:amount toAccount:toAccount.currency.rate];
    fromAccount.balance = [fromAccount.balance decimalNumberBySubtracting:amount];
    toAccount.balance = [toAccount.balance decimalNumberByAdding:transactionAmount];
    success(toAccount, transactionAmount);
}


#pragma mark - Private

- (NSDecimalNumber *)convertFromAccount:(NSDecimalNumber *)fromAccount
                     inputAmount:(NSDecimalNumber *)amount
                       toAccount:(NSDecimalNumber *)toAccount {
    NSDecimalNumber* result = [[amount decimalNumberByDividingBy:fromAccount] decimalNumberByMultiplyingBy:toAccount];
    return result;
}

@end
