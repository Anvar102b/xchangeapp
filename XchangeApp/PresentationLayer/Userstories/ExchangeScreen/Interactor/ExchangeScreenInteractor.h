//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ExchangeScreenInteractorInput.h"

@protocol ExchangeRatesServiceProtocol;
@protocol ExchangeScreenInteractorOutput;
@protocol LocalStorageServiceProtocol;

@interface ExchangeScreenInteractor : NSObject <ExchangeScreenInteractorInput>

@property (nonatomic, weak) id <ExchangeScreenInteractorOutput> presenter;
@property (nonatomic, strong) id <ExchangeRatesServiceProtocol> ratesService;
@property (nonatomic, strong) id <LocalStorageServiceProtocol> storageService;

@end
