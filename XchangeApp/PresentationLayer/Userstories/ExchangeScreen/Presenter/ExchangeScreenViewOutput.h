//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ExchangeScreenViewOutput <NSObject>

- (void)obtainUser;
- (void)obtainCurrencies;
- (NSArray*)accountsArray;
- (NSString *)stringFromAmount;
- (NSString *)stringToAmount;
- (NSString *)rateStringForAccountAtIndex:(NSInteger)index;
- (void)didChangeFromAccountView:(NSInteger)index;
- (void)didChangeToAccountView:(NSInteger)index;
- (void)inputAmountForToAccountDidChangeAmount:(NSDecimalNumber *)amount;
- (void)inputAmountForFromAccountDidChangeAmount:(NSDecimalNumber *)amount;
- (void)exchangeActionTapped;
- (void)willEnterForeground;
- (void) didEnterBackground;

@end
