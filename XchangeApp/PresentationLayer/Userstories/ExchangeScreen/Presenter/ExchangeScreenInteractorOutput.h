//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class User;

@protocol ExchangeScreenInteractorOutput <NSObject>

- (void)connectionFailed:(NSString *)text;
- (void)haveError:(NSString *)text;
- (void)loadedCurrencies:(NSArray *)currenciesArray;
- (void)loadedUser:(User *)user;


@end
