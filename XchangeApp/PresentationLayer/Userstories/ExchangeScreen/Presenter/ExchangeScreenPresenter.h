//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ExchangeScreenViewOutput.h"
#import "ExchangeScreenInteractorOutput.h"

@protocol ExchangeScreenViewInput;
@protocol ExchangeScreenInteractorInput;

@interface ExchangeScreenPresenter : NSObject <ExchangeScreenViewOutput, ExchangeScreenInteractorOutput>

@property (nonatomic, weak) id <ExchangeScreenViewInput> view;
@property (nonatomic, strong) id <ExchangeScreenInteractorInput> interactor;

@end
