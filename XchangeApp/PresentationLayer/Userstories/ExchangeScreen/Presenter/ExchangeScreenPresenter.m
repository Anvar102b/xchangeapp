//
// Created by Anvar Basharov
// Copyright (c) 2017 Anvar Basharov. All rights reserved.
//

#import "ExchangeScreenPresenter.h"
#import "ExchangeScreenInteractor.h"
#import "ExchangeScreenViewController.h"

#import "User.h"
#import "Currency.h"
#import "Account.h"

#import "ExchangeScreenViewInput.h"
#import "ExchangeScreenInteractorInput.h"

@interface ExchangeScreenPresenter ()

@property (nonatomic, strong) User* user;
@property (nonatomic, strong) NSDecimalNumber* inputFromAmount;
@property (nonatomic, strong) NSDecimalNumber* inputToAmount;

@property (nonatomic, strong) Account* fromAccount;
@property (nonatomic, strong) Account* toAccount;

@end

@implementation ExchangeScreenPresenter

#pragma mark - ExchangeScreenViewOutput

- (void)obtainUser {
    [self.interactor loadUser];
}

- (void)obtainCurrencies {
    [self.interactor loadCurrencies];
}

- (NSArray *)accountsArray {
    if (!_fromAccount) {_fromAccount = self.user.accountsArray[0];}
    if (!_toAccount) {_toAccount = self.user.accountsArray[0];}
    return self.user.accountsArray;
}

- (NSString *)stringFromAmount {
    NSString* stringFromAmount;
    if (self.inputFromAmount) {
        stringFromAmount = [NSString stringWithFormat:@"%.02f", self.inputFromAmount.doubleValue];
    }
    return stringFromAmount;
}

- (NSString *)stringToAmount {
    NSString* stringToAmount;
    if (self.inputToAmount) {
        stringToAmount = [NSString stringWithFormat:@"%.02f", self.inputToAmount.doubleValue];
    }
    return stringToAmount;
}

- (NSString *)rateStringForAccountAtIndex:(NSInteger)index {
    Account* account = self.user.accountsArray[index];
    Account* toAccount;
    [account isEqual:self.fromAccount] ? (toAccount = self.toAccount) : (toAccount = self.fromAccount);
    NSDecimalNumber* convertedNum = [self.interactor convertOneUnitFromCurrency:account.currency toCurrency:toAccount.currency];
    NSString* convertOneUnit;
    if (convertedNum) {
        convertOneUnit = [NSString stringWithFormat:@"%@%.02f",toAccount.currency.symbol, convertedNum.doubleValue];
        if ([account isEqual:self.fromAccount]){
            [self.view updateTitle:[NSString stringWithFormat:@"1%@ = %.02f%@",account.currency.symbol, convertedNum.doubleValue, _toAccount.currency.symbol]];
        }
    } else {
        convertOneUnit = nil;
    }
    return convertOneUnit;
}

- (void)didChangeFromAccountView:(NSInteger)index {
    self.fromAccount = self.user.accountsArray[index];
    [self inputAmountForFromAccountDidChangeAmount:self.inputFromAmount];
    [self.view reloadCells:FromAccountCellType];
}

- (void)didChangeToAccountView:(NSInteger)index {
    self.toAccount = self.user.accountsArray[index];
    [self inputAmountForToAccountDidChangeAmount:self.inputToAmount];
    [self.view reloadCells:ToAccountCellType];
}

-(void)inputAmountForFromAccountDidChangeAmount:(NSDecimalNumber *)amount {
    self.inputFromAmount = amount;
    if (amount) {
        self.inputToAmount = [self.interactor convertFromCurrency:self.fromAccount inputAmount:self.inputFromAmount toAccount:self.toAccount];
    } else {
        self.inputToAmount = nil;
    }
    [self.view reloadCells:ToAccountCellType];
}

-(void)inputAmountForToAccountDidChangeAmount:(NSDecimalNumber *)amount {
    self.inputToAmount = amount;
    if (amount) {
        self.inputFromAmount = [self.interactor convertFromCurrency:self.toAccount inputAmount:self.inputToAmount toAccount:self.fromAccount];
    } else {
        self.inputFromAmount = nil;
    }
    [self.view reloadCells:FromAccountCellType];
}

- (void)exchangeActionTapped {
    [self.interactor transactionFromAccount:self.fromAccount inputAmount:self.inputFromAmount toAccount:self.toAccount success:^(Account *account, NSNumber *convertedNum) {
        NSMutableString* mString = [NSMutableString stringWithString:@"\n\nAvailable accounts:\n"];
        
        for (Account* account in self.user.accountsArray) {
            [mString appendString:[NSString stringWithFormat:@"%@: %@%.02f\n", account.currency.code, account.currency.symbol, account.balance.doubleValue]];
        }
        NSString* successString = [NSString stringWithFormat:@"Receipt %@%.02f to account %@. Available balance: %@%.02f",
                                   account.currency.symbol,
                                   convertedNum.doubleValue,
                                   account.currency.code,
                                   account.currency.symbol,
                                   account.balance.doubleValue];
        [mString insertString:successString atIndex:0];
       
        [self.view showAlertWithMessage:mString];
        [self.interactor saveUser:self.user];
        self.inputFromAmount = nil; self.inputToAmount = nil;
        [self.view reloadCells:FromAccountCellType];
        [self.view reloadCells:ToAccountCellType];
    } failure:^(NSError *error) {
        [self.view showAlertWithMessage:error.localizedDescription];
    }];
    
}

- (void)willEnterForeground {
    if (!self.interactor.isTimerValid) {
        [self.interactor startUpdatingCurrencies];
    }
}
- (void) didEnterBackground {
    if (self.interactor.isTimerValid) {
        [self.interactor invalidateTimer];
    }
}

#pragma mark - ExchangeScreenInteractorOutput

- (void)loadedUser:(User *)user {
    _user = user;
}

- (void)haveError:(NSString *)text {
    [self.view showAlertWithMessage:text];
}

- (void)connectionFailed:(NSString *)text {
    [self.view showNoInternetAlert:text];
}

- (void)loadedCurrencies:(NSArray *)currenciesArray {
    
    for (Currency *currency in currenciesArray) {
        for (Account* account in self.user.accountsArray) {
            if (currency.currencyType == account.currency.currencyType) {
                account.currency = currency;
            }
        }
    }
    [self.view reloadCells:FromAccountCellType];
    [self.view reloadCells:ToAccountCellType];
    
    if (self.interactor.isTimerValid == NO) {
        [self.interactor startUpdatingCurrencies];
    }
}

@end
