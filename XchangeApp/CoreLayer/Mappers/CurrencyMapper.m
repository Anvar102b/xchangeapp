//
//  AccountMapper.m
//  XchangeApp
//
//  Created by Anvar Basharov on 24.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "CurrencyMapper.h"
#import "Currency.h"

#import "Types.h"

@implementation CurrencyMapper

+ (NSArray<Currency*>*)mapRatesArrayWithDict:(NSDictionary*)dict {
    
    NSArray* currencies = dict[@"gesmes:Envelope"][@"Cube"][@"Cube"][@"CubesArray"];
    NSMutableArray* rates = [NSMutableArray array];
    
    for (NSDictionary* dict in currencies) {
        if ([[self necessaryCurrencies] containsObject:dict[@"currency"]]) {
            Currency* currency = [Currency new];
            currency.code = dict[@"currency"];
            currency.currencyType = [Currency currencyTypeForString:dict[@"currency"]];
            currency.symbol = [Currency symbolForCurrency:currency.currencyType];
            NSString* rate = dict[@"rate"];
            currency.rate = [NSDecimalNumber decimalNumberWithString:rate];
            [rates addObject:currency];
        }
    }
    return rates;
}

+ (BOOL)isNecessaryCurrency:(NSString *)currStr {
    if ([[self necessaryCurrencies] containsObject:currStr]) {
        return YES;
    }
    return NO;
}

+ (NSSet *)necessaryCurrencies {
    return [NSSet setWithArray:@[@"EUR", @"USD", @"GBP"]];
    
}

@end
