//
//  IsConnectedToNetwork.m
//  XchangeApp
//
//  Created by Anvar Basharov on 24.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "IsConnectedToNetwork.h"
#import "Reachability.h"

@implementation IsConnectedToNetwork

+ (BOOL) isConnectedToNetwork {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}

@end
