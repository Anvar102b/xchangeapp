//
//  URLRequest.h
//  XchangeApp
//
//  Created by Anvar Basharov on 23.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    GET,
    POST,
    PATCH,
    DELETE,
} MBRequestType;

@interface URLRequest : NSMutableURLRequest

- (instancetype)initWithMethod:(MBRequestType)type urlString:(NSString *)urlString parameters:(NSDictionary *)parameters;


@end
