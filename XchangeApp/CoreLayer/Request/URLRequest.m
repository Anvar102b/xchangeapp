//
//  URLRequest.m
//  XchangeApp
//
//  Created by Anvar Basharov on 23.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "URLRequest.h"
#import "NSString+URL.h"

const NSTimeInterval DefaultTimeout = 15.0;

@implementation URLRequest

- (instancetype)initWithMethod:(MBRequestType)type urlString:(NSString *)urlString parameters:(NSDictionary *)parameters {
    self = [super initWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:DefaultTimeout];
    if (self) {
        [self setupWithMethod:type urlString:urlString parameters:parameters];
    }
    return self;
}

#pragma mark - Private Methods

- (void)setupWithMethod:(MBRequestType)type urlString:(NSString*)urlString parameters:(NSDictionary*)parameters {
    NSString *httpMethod;
    switch (type) {
        case GET: {
            httpMethod = @"GET";
            if (parameters) {
                urlString = [NSString stringWithFormat:@"%@%@", urlString, [NSString urlWithParameters:parameters]];
                urlString = [urlString substringToIndex:urlString.length - 1];
                urlString = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
                self.URL = [NSURL URLWithString:urlString];
            }
            break;
        }
        case POST: {
            httpMethod = @"POST";
            break;
        }
        case PATCH: {
            httpMethod = @"PATCH";
            break;
        }
        case DELETE: {
            httpMethod = @"DELETE";
            break;
        }
        default:
            break;
    }
    
    [self setHTTPMethod:httpMethod];
    if (type != GET) {
        if (parameters) {
            NSError *error;
            NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:&error];
            [self setHTTPBody:postData];
        }
    }
}


@end
