//
//  WebClient.h
//  XchangeApp
//
//  Created by Anvar Basharov on 23.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Error;

@interface WebClient : NSObject

- (instancetype)initWithBaseUrl:(NSString *)urlString NS_DESIGNATED_INITIALIZER;

- (NSURLSessionDataTask *)makeRequest:(NSURLRequest *)request completion:(void (^)(NSData *data, NSURLResponse *response, Error *error))completion;

+ (NSString *)baseUrl;

@end
