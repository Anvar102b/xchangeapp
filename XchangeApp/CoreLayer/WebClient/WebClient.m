//
//  WebClient.m
//  XchangeApp
//
//  Created by Anvar Basharov on 23.09.17.
//  Copyright © 2017 Anvar Basharov. All rights reserved.
//

#import "WebClient.h"
#import "Error.h"
#import "IsConnectedToNetwork.h"
#import <UIKit/UIKit.h>

NSString *baseURLString;

@implementation WebClient

- (instancetype)init {
    return [self initWithBaseUrl:[WebClient baseUrl]];
}

- (instancetype)initWithBaseUrl:(NSString *)urlString {
    self = [super init];
    if (self) {
        baseURLString = urlString;
        
    }
    return self;
}

+ (NSString *)baseUrl {
    if (!baseURLString) {
        baseURLString = @"http://www.ecb.europa.eu/";
    }
    return baseURLString;
}

#pragma mark - Public

- (NSURLSessionDataTask *)makeRequest:(NSURLRequest *)request completion:(void (^)(NSData *data, NSURLResponse *response, Error *error))completion {
    
    if (![IsConnectedToNetwork isConnectedToNetwork]) {
        Error *error = [Error new];
        error.errorType = NoInternetConnection;
        completion(nil,nil,error);
    }
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    NSURLSessionDataTask* task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{ [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO]; });
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if (httpResponse){
            if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 300) {
                completion(data, response, nil);
            } else {
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                Error *serviceError = [[Error alloc] initWithDomain:error.domain code:error.code userInfo:error.userInfo];
                serviceError.errorType = Custom;
                if (json) {
                    [serviceError setMessage:[NSString stringWithFormat:@"%@", json]];
                } else {
                    [serviceError setMessage:@"Something went wrong"];
                }
                completion(nil,response,serviceError);
            }
        }
        if (error) {
            Error* serviceError = [[Error alloc] initWithDomain:error.domain code:error.code userInfo:error.userInfo];
            serviceError.errorType = Custom;
            [serviceError setMessage:error.localizedDescription];
            completion(nil,nil,serviceError);
        }
    }];
    [task resume];
    
    return task;
}


@end
